package cn.com.eship.smppserver;

public class DemoTest {
    public static void main(String[] args){

        Thread thread1 = new Thread(new ThreadFirst());
        thread1.start();

    }
}

class ThreadFirst implements Runnable{
    private Boolean flag = new Boolean(false);

    public ThreadFirst(){
        new Thread(new ThreadSecond(flag)).start();
    }

    /**
     * When an object implementing interface <code>Runnable</code> is used
     * to create a thread, starting the thread causes the object's
     * <code>run</code> method to be called in that separately executing
     * thread.
     * <p>
     * The general contract of the method <code>run</code> is that it may
     * take any action whatsoever.
     *
     * @see Thread#run()
     */
    @Override
    public void run() {
        while (true){
            try {
                Thread.sleep(3000);
                this.flag = new Boolean(true);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}

class ThreadSecond implements Runnable{
    private Boolean flag;

    public ThreadSecond(Boolean flag){
        this.flag = flag;
    }

    /**
     * When an object implementing interface <code>Runnable</code> is used
     * to create a thread, starting the thread causes the object's
     * <code>run</code> method to be called in that separately executing
     * thread.
     * <p>
     * The general contract of the method <code>run</code> is that it may
     * take any action whatsoever.
     *
     * @see Thread#run()
     */
    @Override
    public void run() {
        while (true){
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(this.flag);
        }
    }
}
