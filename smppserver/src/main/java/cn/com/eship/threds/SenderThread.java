package cn.com.eship.threds;

import cn.com.eship.utils.SMPPStruct;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.Map;
import java.util.concurrent.BlockingQueue;

public class SenderThread extends Thread {
    private BlockingQueue<Socket> socketQueue;
    private BlockingQueue<Map<String, String>> msgQueue;

    public SenderThread(BlockingQueue<Socket> socketQueue, BlockingQueue<Map<String, String>> msgQueue) {
        this.socketQueue = socketQueue;
        this.msgQueue = msgQueue;
    }

    @Override
    public void run() {
        Socket socket = null;
        Map<String, String> msgMap = null;
        while (true) {
            try {
                if (socket != null && socket.isConnected()) {
                    if (msgMap != null) {
                        //上次发送失败
                        writeSubmitData(socket, msgMap.get("src"), msgMap.get("msg"));
                        System.out.println("send msg successful src=" + msgMap.get("src") + ",msg=" + msgMap.get("msg"));
                    }
                    msgMap = msgQueue.take();
                    writeSubmitData(socket, msgMap.get("src"), msgMap.get("msg"));
                    System.out.println("send msg successful src=" + msgMap.get("src") + ",msg=" + msgMap.get("msg"));
                    msgMap = null;
                } else {
                    socket = socketQueue.take();
                }
            } catch (Exception e) {
                try {
                    socket = socketQueue.take();
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                }
            }
        }

    }


    private Map<String, Object> getData(Socket socket) throws Exception {
        DataInputStream dataInputStream = new DataInputStream(socket.getInputStream());
        byte[] data = new byte[4096];
        dataInputStream.read(data);
        Map<String, Object> map = SMPPStruct.unpackCommonData(data);
        return map;
    }

    private void writeResponse(Socket socket, String commandId, int sequceNo) throws Exception {
        DataOutputStream dataOutputStream = new DataOutputStream(socket.getOutputStream());
        byte[] data1 = SMPPStruct.packCommonData(commandId, sequceNo);
        dataOutputStream.write(data1);
        dataOutputStream.flush();
    }

    private void writeSubmitData(Socket socket, String src, String msg) throws Exception {
        byte[] data3 = SMPPStruct.packSubmitData(src, msg);
        DataOutputStream dataOutputStream = new DataOutputStream(socket.getOutputStream());
        dataOutputStream.write(data3);
        dataOutputStream.flush();
    }
}
