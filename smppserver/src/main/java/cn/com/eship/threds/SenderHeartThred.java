package cn.com.eship.threds;

import cn.com.eship.utils.SMPPStruct;

import java.io.DataOutputStream;
import java.net.Socket;

public class SenderHeartThred extends Thread {
    public Socket socket;
    private int sequceNo;

    public SenderHeartThred(Socket socket, int sequceNo) {
        this.socket = socket;
        this.sequceNo = sequceNo;
    }

    /**
     * If this thread was constructed using a separate
     * <code>Runnable</code> run object, then that
     * <code>Runnable</code> object's <code>run</code> method is called;
     * otherwise, this method does nothing and returns.
     * <p>
     * Subclasses of <code>Thread</code> should override this method.
     *
     * @see #start()
     * @see #stop()
     * @see #Thread(ThreadGroup, Runnable, String)
     */
    @Override
    public void run() {
        while (true) {
            try {
                writeResponse(socket, "80000001", sequceNo);
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void writeResponse(Socket socket, String commandId, int sequceNo) throws Exception {
        DataOutputStream dataOutputStream = new DataOutputStream(socket.getOutputStream());
        byte[] data1 = SMPPStruct.packCommonData(commandId, sequceNo);
        dataOutputStream.write(data1);
        dataOutputStream.flush();
    }
}
