package cn.com.eship.threds;

import cn.com.eship.utils.SMPPStruct;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Map;
import java.util.concurrent.BlockingQueue;

public class UnknowServerThread extends Thread {
    public Socket socket;
    private int sequceNo;

    public UnknowServerThread(Socket socket, int sequceNo) {
        this.socket = socket;
        this.sequceNo = sequceNo;
    }

    @Override
    public void run() {
        while (true) {
            try {
                writeResponse(socket, "80000002", sequceNo);
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    private Map<String, Object> getData(Socket socket) throws Exception {
        DataInputStream dataInputStream = new DataInputStream(socket.getInputStream());
        byte[] data = new byte[4096];
        dataInputStream.read(data);
        Map<String, Object> map = SMPPStruct.unpackCommonData(data);
        return map;
    }

    private void writeResponse(Socket socket, String commandId, int sequceNo) throws Exception {
        DataOutputStream dataOutputStream = new DataOutputStream(socket.getOutputStream());
        byte[] data1 = SMPPStruct.packCommonData(commandId, sequceNo);
        dataOutputStream.write(data1);
        dataOutputStream.flush();
    }

    private void writeSubmitData(Socket socket, String src, String msg) throws Exception {
        byte[] data3 = SMPPStruct.packSubmitData(src, msg);
        DataOutputStream dataOutputStream = new DataOutputStream(socket.getOutputStream());
        dataOutputStream.write(data3);
        dataOutputStream.flush();
    }
}
