package cn.com.eship.threds;

import cn.com.eship.utils.SMPPStruct;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Map;
import java.util.concurrent.BlockingQueue;

public class SenderServerThread extends Thread {
    private ServerSocket serverSocket;
    private BlockingQueue<Socket> socketQueue;

    public SenderServerThread(BlockingQueue<Socket> socketQueue) {
        try {
            this.socketQueue = socketQueue;
            serverSocket = new ServerSocket(5000);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        while (true) {
            try {
                Socket tempSocket = serverSocket.accept();
                Map<String, Object> map = getData(tempSocket);
                switch ((int) map.get("commandID")) {
                    case 1: {
                        writeResponse(tempSocket, "80000001", (int) map.get("sequenceNo"));
                        System.out.println(map);
                        this.socketQueue.put(tempSocket);
                        System.out.println("connected ok");
                        new SenderHeartThred(tempSocket, (int) map.get("sequenceNo")).start();
                        break;
                    }
                    case 2: {
                        writeResponse(tempSocket, "80000002", (int) map.get("sequenceNo"));
                        System.out.println(map);
                        new UnknowServerThread(tempSocket, (int) map.get("sequenceNo")).start();
                        break;
                    }
                }

            } catch (Exception e) {
                System.out.println(e.getMessage());

            }

        }
    }


    private Map<String, Object> getData(Socket socket) throws Exception {
        DataInputStream dataInputStream = new DataInputStream(socket.getInputStream());
        byte[] data = new byte[4096];
        dataInputStream.read(data);
        Map<String, Object> map = SMPPStruct.unpackCommonData(data);
        return map;
    }

    private void writeResponse(Socket socket, String commandId, int sequceNo) throws Exception {
        DataOutputStream dataOutputStream = new DataOutputStream(socket.getOutputStream());
        byte[] data1 = SMPPStruct.packCommonData(commandId, sequceNo);
        dataOutputStream.write(data1);
        dataOutputStream.flush();
    }

    private void writeSubmitData(Socket socket, String src, String msg) throws Exception {
        byte[] data3 = SMPPStruct.packSubmitData(src, msg);
        DataOutputStream dataOutputStream = new DataOutputStream(socket.getOutputStream());
        dataOutputStream.write(data3);
        dataOutputStream.flush();
    }
}
