package cn.com.eship.utils;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class SMPPStruct {
    public static Map<String,Object> unpackLoginData(byte[] data) throws Exception{
        int commandLenth = PacketUtils.bytesToInt2(Arrays.copyOfRange(data,0,4),0);
        int commandID = PacketUtils.bytesToInt2(Arrays.copyOfRange(data,4,8),0);
        int  commandStatus = PacketUtils.bytesToInt2(Arrays.copyOfRange(data,8,12),0);
        int sequenceNo = PacketUtils.bytesToInt2(Arrays.copyOfRange(data,12,16),0);

        String systemId = PacketUtils.bytesToString(Arrays.copyOfRange(data,16,23));
        String password = PacketUtils.bytesToString(Arrays.copyOfRange(data,23,30));
        String systemType = PacketUtils.bytesToString(Arrays.copyOfRange(data,30,34));

        Map<String,Object> map = new HashMap<>();
        map.put("commandLenth",commandLenth);
        map.put("commandID",commandID);
        map.put("commandStatus",commandStatus);
        map.put("sequenceNo",sequenceNo);
        map.put("systemId",systemId);
        map.put("password",password);
        map.put("systemType",systemType);

        return map;
    }

    public static byte[] packLoginData() throws Exception {
        //cammand长度 4个字节
        byte[] commandLenth = null;
        //Command_ID 4个字节
        byte[] commandID = PacketUtils.intToByteArray(2);
        //commandStatus消息状态 4个字节
        byte[] commandStatus = PacketUtils.intToByteArray(0);
        //sequenceNo 4个字节 范围 0 到 FFFFFFFF 严格递增
        byte[] sequenceNo = PacketUtils.intToByteArray(0);


        //封装消息体 Optional Message Body
        byte[] systemId = PacketUtils.toCStringBytes("zx_gateway1");
        byte[] password = PacketUtils.toCStringBytes("zx_gateway1");
        byte[] systemType = PacketUtils.toCStringBytes("web");
        byte[] interfaceVersion = PacketUtils.hexStringToByte("03");
        byte[] ton = PacketUtils.hexStringToByte("00");
        byte[] api = PacketUtils.hexStringToByte("00");
        byte[] addressRange = new byte[]{'\0'};
        int packageLenth = commandID.length + commandStatus.length + sequenceNo.length + systemId.length + password.length + systemType.length + interfaceVersion.length + ton.length + api.length + addressRange.length + 4;
        commandLenth = PacketUtils.intToByteArray(packageLenth);


        byte[] packet = PacketUtils.byteMergerAll(commandLenth, commandID, commandStatus, sequenceNo, systemId, password, systemType, interfaceVersion, ton, api, addressRange);
        return packet;
    }


    public static byte[] packSubmitData(String src,String msg) throws Exception {
        //cammand长度 4个字节
        byte[] commandLenth = null;
        //Command_ID 4个字节
        byte[] commandID = PacketUtils.intToByteArray(5);
        //commandStatus消息状态 4个字节
        byte[] commandStatus = PacketUtils.intToByteArray(0);
        //sequenceNo 4个字节 范围 0 到 FFFFFFFF 严格递增
        byte[] sequenceNo = PacketUtils.intToByteArray(1);

        //服务类型 由任意8位字节组成数据流串
        byte[] serviceType = new byte[]{'\0'};
        byte[] sourceAddressTon = PacketUtils.hexStringToByte("01");
        byte[] sourceAddressNpi = PacketUtils.hexStringToByte("01");
        byte[] sourceAddress = PacketUtils.toCStringBytes(src);
        byte[] destAddressTon = PacketUtils.hexStringToByte("01");
        byte[] destAddressNpi = PacketUtils.hexStringToByte("01");
        byte[] destinationAddress = PacketUtils.toCStringBytes("10690700367");
        byte[] esmClass = PacketUtils.hexStringToByte("00");
        byte[] protocolId = PacketUtils.hexStringToByte("00");
        byte[] priorityFlag = PacketUtils.hexStringToByte("00");
        byte[] scheduleDeliveryTime = new byte[]{'\0'};
        byte[] validityPeroid = new byte[]{'\0'};
        byte[] registeredDeliveryFlag = PacketUtils.hexStringToByte("01");
        byte[] replaceIfPresentFlag = PacketUtils.hexStringToByte("00");
        byte[] dataCoding = PacketUtils.hexStringToByte("00");
        byte[] smDefaultMsgId = PacketUtils.hexStringToByte("00");
        byte[] smLength = PacketUtils.hexStringToByte("04");
        byte[] shortMessageText = PacketUtils.toCStringBytes(msg);

        int packetLenth = commandID.length + commandStatus.length + sequenceNo.length + serviceType.length + sourceAddressTon.length + sourceAddressNpi.length + sourceAddress.length + destAddressTon.length +
                destAddressNpi.length + destinationAddress.length + esmClass.length + protocolId.length + priorityFlag.length +
                scheduleDeliveryTime.length + validityPeroid.length + registeredDeliveryFlag.length + replaceIfPresentFlag.length + dataCoding.length +
                smDefaultMsgId.length + smLength.length + shortMessageText.length + 4;
        commandLenth = PacketUtils.intToByteArray(packetLenth);

        byte[] submitSMSPacket = PacketUtils.byteMergerAll(commandLenth, commandID, commandStatus, sequenceNo, serviceType, sourceAddressTon, sourceAddressNpi, sourceAddress, destAddressTon, destAddressNpi, destinationAddress, esmClass, protocolId, priorityFlag, scheduleDeliveryTime, validityPeroid, registeredDeliveryFlag, replaceIfPresentFlag, dataCoding, smDefaultMsgId, smLength, shortMessageText);
        return submitSMSPacket;
    }


    private static Map<String, Object> unpackSubmitData(byte[] data) throws Exception {
        int commandLenth = PacketUtils.bytesToInt2(Arrays.copyOfRange(data, 0, 4), 0);
        int commandID = PacketUtils.bytesToInt2(Arrays.copyOfRange(data, 4, 8), 0);
        int commandStatus = PacketUtils.bytesToInt2(Arrays.copyOfRange(data, 8, 12), 0);
        int sequenceNo = PacketUtils.bytesToInt2(Arrays.copyOfRange(data, 12, 16), 0);


        //服务类型 由任意8位字节组成数据流串
        String serviceType = PacketUtils.bytesToString(Arrays.copyOfRange(data, 16, 17));
        byte sourceAddressTon = PacketUtils.bytesToByteWith16Radix(Arrays.copyOfRange(data, 17, 18));
        byte sourceAddressNpi = PacketUtils.bytesToByteWith16Radix(Arrays.copyOfRange(data, 18, 19));
        String sourceAddress = PacketUtils.bytesToString(Arrays.copyOfRange(data, 19, 31));

        byte destAddressTon = PacketUtils.bytesToByteWith16Radix(Arrays.copyOfRange(data, 31, 32));
        byte destAddressNpi = PacketUtils.bytesToByteWith16Radix(Arrays.copyOfRange(data, 32, 33));
        String destinationAddress = PacketUtils.bytesToString(Arrays.copyOfRange(data, 33, 45));
        byte esmClass = PacketUtils.bytesToByteWith16Radix(Arrays.copyOfRange(data, 45, 46));
        byte protocolId = PacketUtils.bytesToByteWith16Radix(Arrays.copyOfRange(data, 46, 47));
        byte priorityFlag = PacketUtils.bytesToByteWith16Radix(Arrays.copyOfRange(data, 47, 48));
        String scheduleDeliveryTime = null;
        String validityPeroid = null;
        byte registeredDeliveryFlag = PacketUtils.bytesToByteWith16Radix(Arrays.copyOfRange(data, 50, 51));
        byte replaceIfPresentFlag = PacketUtils.bytesToByteWith16Radix(Arrays.copyOfRange(data, 51, 52));
        byte dataCoding = PacketUtils.bytesToByteWith16Radix(Arrays.copyOfRange(data, 52, 53));
        byte smDefaultMsgId = PacketUtils.bytesToByteWith16Radix(Arrays.copyOfRange(data, 53, 54));
        byte smLength = PacketUtils.bytesToByteWith16Radix(Arrays.copyOfRange(data, 54, 55));
        String shortMessageText = PacketUtils.bytesToString(Arrays.copyOfRange(data, 55, 60));

        Map<String, Object> map = new HashMap<>();
        map.put("commandLenth", commandLenth);
        map.put("commandID", commandID);
        map.put("commandStatus", commandStatus);
        map.put("sequenceNo", sequenceNo);
        map.put("serviceType", serviceType);
        map.put("sourceAddressTon", sourceAddressTon);
        map.put("sourceAddressNpi", sourceAddressNpi);
        map.put("sourceAddress", sourceAddress);
        map.put("destAddressTon", destAddressTon);
        map.put("destAddressNpi", destAddressNpi);
        map.put("destinationAddress", destinationAddress);
        map.put("esmClass", esmClass);
        map.put("protocolId", protocolId);
        map.put("priorityFlag", priorityFlag);
        map.put("scheduleDeliveryTime", scheduleDeliveryTime);
        map.put("validityPeroid", validityPeroid);
        map.put("registeredDeliveryFlag", registeredDeliveryFlag);
        map.put("replaceIfPresentFlag", replaceIfPresentFlag);
        map.put("dataCoding", dataCoding);
        map.put("smDefaultMsgId", smDefaultMsgId);
        map.put("smLength", smLength);
        map.put("shortMessageText", shortMessageText);
        return map;
    }


    public static byte[] packCommonData(String commandId,int sequceNo){
        //cammand长度 4个字节
        byte[] commandLenth = null;
        //Command_ID 4个字节
        byte[] commandID = PacketUtils.hexStringToByte(commandId);
        //commandStatus消息状态 4个字节
        byte[] commandStatus = PacketUtils.intToByteArray(0);
        //sequenceNo 4个字节 范围 0 到 FFFFFFFF 严格递增
        byte[] sequenceNo = PacketUtils.intToByteArray(sequceNo);
        int packetLenth = commandID.length + commandStatus.length + sequenceNo.length + 4;
        commandLenth = PacketUtils.intToByteArray(packetLenth);
        byte[] data = PacketUtils.byteMergerAll(commandLenth,commandID,commandStatus,sequenceNo);
        return data;
    }


    public static Map<String,Object> unpackCommonData(byte[] data){
        int commandLenth = PacketUtils.bytesToInt2(Arrays.copyOfRange(data, 0, 4), 0);
        int commandID = PacketUtils.bytesToInt2(Arrays.copyOfRange(data, 4, 8), 0);
        int commandStatus = PacketUtils.bytesToInt2(Arrays.copyOfRange(data, 8, 12), 0);
        int sequenceNo = PacketUtils.bytesToInt2(Arrays.copyOfRange(data, 12, 16), 0);
        Map<String, Object> map = new HashMap<>();
        map.put("commandLenth", commandLenth);
        map.put("commandID", commandID);
        map.put("commandStatus", commandStatus);
        map.put("sequenceNo", sequenceNo);
        return map;
    }






}
