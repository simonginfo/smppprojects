package cn.com.eship.mq;

import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

@Component
public class MsgQueueFactory {
    private BlockingQueue<Map<String, String>> msgQueue = new LinkedBlockingQueue<Map<String, String>>();

    public BlockingQueue<Map<String, String>> getMsgQueue() {
        return msgQueue;
    }
}
