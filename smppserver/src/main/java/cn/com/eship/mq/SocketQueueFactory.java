package cn.com.eship.mq;

import org.springframework.stereotype.Component;

import java.net.Socket;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

@Component
public class SocketQueueFactory {
    private BlockingQueue<Socket> socketQueue = new LinkedBlockingQueue<>(1);

    public BlockingQueue<Socket> getSocketQueue(){
        return socketQueue;
    }
}
