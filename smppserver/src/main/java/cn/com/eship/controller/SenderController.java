package cn.com.eship.controller;

import cn.com.eship.mq.MsgQueueFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("sender")
public class SenderController {
    @Autowired
    private MsgQueueFactory msgQueueFactory;

    @RequestMapping("senderMsg")
    public @ResponseBody
    String senderMsg(String src, String msg) throws Exception {
        Map<String, String> msgMap = new HashMap<>();
        msgMap.put("src", src);
        msgMap.put("msg", msg);
        msgQueueFactory.getMsgQueue().put(msgMap);
        return "1";
    }


    @RequestMapping("getQueueSize")
    public @ResponseBody
    int getQueueSize() {
        return msgQueueFactory.getMsgQueue().size();
    }
}
