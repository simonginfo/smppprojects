package cn.com.eship;

import cn.com.eship.mq.MsgQueueFactory;
import cn.com.eship.mq.SocketQueueFactory;
import cn.com.eship.threds.SenderServerThread;
import cn.com.eship.threds.SenderThread;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import java.net.Socket;
import java.util.Map;
import java.util.concurrent.BlockingQueue;

@SpringBootApplication
public class SmppserverApplication {
    public static void main(String[] args) {
        ApplicationContext applicationContext = SpringApplication.run(SmppserverApplication.class, args);
        MsgQueueFactory msgQueueFactory = applicationContext.getBean(MsgQueueFactory.class);
        SocketQueueFactory socketQueueFactory = applicationContext.getBean(SocketQueueFactory.class);
        BlockingQueue<Map<String, String>> msgQueue = msgQueueFactory.getMsgQueue();
        BlockingQueue<Socket> socketQueue = socketQueueFactory.getSocketQueue();
        new SenderServerThread(socketQueue).start();
        new SenderThread(socketQueue, msgQueue).start();
        System.out.println("esmc is running");
    }
}
