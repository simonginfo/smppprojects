package cn.com.eship.service.impl;


import cn.com.eship.models.Phone;
import cn.com.eship.models.ValidationRecord;
import cn.com.eship.repository.PhoneRepository;
import cn.com.eship.repository.ValidationRecordRepository;
import cn.com.eship.service.SystemService;
import org.apache.commons.lang.StringUtils;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Service
@EnableTransactionManagement
public class SystemServiceImpl implements SystemService {
    @Autowired
    private ValidationRecordRepository validationRecordRepository;
    @Autowired
    private PhoneRepository phoneRepository;

    @Transactional
    @Override
    public void replaceValidationRecord(String phoneNum, String msg) {
        validationRecordRepository.updateValidationRecordStatus(phoneNum, msg);
    }

    @Transactional
    @Override
    public List<ValidationRecord> findInitValidationRecordList(String sessionId, int limit) {
        PageRequest pageRequest = new PageRequest(0, limit);
        Page<Phone> phones = phoneRepository.findAll(pageRequest);
        List<Phone> phoneList = phones.getContent();
        List<ValidationRecord> validationRecords = new ArrayList<>();
        for (Phone phone : phoneList) {
            ValidationRecord validationRecord = new ValidationRecord();
            validationRecord.setId(phone.getId());
            validationRecord.setPhoneNum(phone.getPhoneNum());
            validationRecord.setSessionId(sessionId);
            validationRecord.setUpdateDate(new Date());
            validationRecords.add(validationRecord);
        }
        phoneRepository.delete(phoneList);
        validationRecordRepository.save(validationRecords);
        return validationRecords;
    }

    @Override
    public List<ValidationRecord> findValidationRecordList(String sessionId) {
        List<ValidationRecord> validationRecordList = validationRecordRepository.findBySessionId(sessionId);
        return validationRecordList;
    }

    @Transactional
    @Override
    public List<ValidationRecord> releasePhoneList(String sessionId) {
        validationRecordRepository.updateStastusBySessionId(sessionId);
        List<ValidationRecord> validationRecordList = validationRecordRepository.findBySessionId(sessionId);
        return validationRecordList;
    }

    @Transactional
    @Override
    public void savePhones(List<String> phones) throws Exception {
        StringBuffer urlBuffer = new StringBuffer("http://47.105.32.206/API/Add.ashx?APPID=xiaosan&APPKEY=AC3A46002A712F98EF84C736AF646EDF&PhoneNo=");
        CloseableHttpClient client = HttpClients.createDefault();
        List<Phone> phoneList = new ArrayList<>();
        for (String phone : phones) {
            if (StringUtils.isNotBlank(phone)) {
                Phone phone1 = new Phone();
                phone1.setId(UUID.randomUUID().toString().replace("-", ""));
                phone1.setPhoneNum(phone);
                phoneList.add(phone1);
                urlBuffer.append(phone + ",");
            }
        }
        HttpGet get = new HttpGet(urlBuffer.deleteCharAt(urlBuffer.length() - 1).toString());
        CloseableHttpResponse response = client.execute(get);
        response.close();
        client.close();

        phoneRepository.save(phoneList);
    }

    @Transactional
    @Override
    public void deleteByPhone(String phone) {
        phoneRepository.deleteByPhoneNum(phone);
    }
}
