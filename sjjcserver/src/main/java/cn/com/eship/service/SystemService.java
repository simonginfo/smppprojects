package cn.com.eship.service;

import cn.com.eship.models.ValidationRecord;

import java.util.List;

public interface SystemService {
    void replaceValidationRecord(String phoneNum,String msg);

    //初始化获取手机号码
    List<ValidationRecord> findInitValidationRecordList(String sessionId,int limit);


    //轮训手机号码状态
    List<ValidationRecord> findValidationRecordList(String sessionId);


    //释放手机号码
    List<ValidationRecord> releasePhoneList(String sessionId);

    void savePhones(List<String> phones) throws Exception;

    void deleteByPhone(String phone);
}
