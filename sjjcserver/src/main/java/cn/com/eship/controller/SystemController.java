package cn.com.eship.controller;

import cn.com.eship.service.SystemService;
import cn.com.eship.models.ValidationRecord;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("system")
public class SystemController {
    @Autowired
    private SystemService systemService;

    @RequestMapping("reciveMsg")
    public @ResponseBody
    String reciveMsg(String phoneNum, String msg) {
        systemService.replaceValidationRecord(phoneNum, msg);
        return "1";
    }


    @RequestMapping("reciveMultiMsg")
    public @ResponseBody
    String reciveMultiMsg(HttpServletRequest request) throws Exception {
        String queryString = request.getQueryString();
        StringBuffer returnedMsg = new StringBuffer();
        if (StringUtils.isNotBlank(queryString)) {
            String[] msgArray = queryString.split("/");
            for (String msgStr : msgArray) {
                String[] msg = msgStr.split("&");
                String phoneNo = msg[0].split("=")[1];
                String msgContent = msg[1].split("=")[1];
                if (StringUtils.isNotBlank(phoneNo) && StringUtils.isNotBlank(msgContent)) {
                    systemService.replaceValidationRecord(phoneNo, msgContent);
                    returnedMsg.append("1&");
                } else {
                    returnedMsg.append("0&");
                }

            }

        }
        return returnedMsg.deleteCharAt(returnedMsg.length() - 1).toString();
    }

    @RequestMapping("loadsPhones")
    public @ResponseBody
    List<ValidationRecord> loadsPhones(String sessionId, int limit) {
        List<ValidationRecord> validationRecordList = systemService.findInitValidationRecordList(sessionId, limit);
        return validationRecordList;
    }


    /**
     * 释放手机号并返回最终结果
     *
     * @param sessionId
     * @return
     */
    @RequestMapping("releasePhones")
    public @ResponseBody
    List<ValidationRecord> releasePhones(String sessionId) {
        return systemService.releasePhoneList(sessionId);
    }

    @RequestMapping("loadsValidationRecordList")
    public @ResponseBody
    List<ValidationRecord> loadsValidationRecordList(String sessionId) {
        List<ValidationRecord> validationRecordList = systemService.findValidationRecordList(sessionId);
        return validationRecordList;
    }


    @PostMapping("/upload")
    @ResponseBody
    public String upload(@RequestParam("file") MultipartFile file) throws Exception {
        if (!file.isEmpty()) {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(file.getInputStream()));
            List<String> phoneList = new ArrayList<>();
            String phoneNum = null;
            while ((phoneNum = bufferedReader.readLine()) != null) {
                phoneList.add(phoneNum);
            }
            systemService.savePhones(phoneList);
        }
        return "1";

    }

    @RequestMapping("CallbackJDJC")
    public @ResponseBody
    String callback(String phone, String status, String province) {
        if (StringUtils.isNotBlank(status) && "2".equals(status.trim())) {
            systemService.deleteByPhone(phone);
        }
        return "1";
    }
}