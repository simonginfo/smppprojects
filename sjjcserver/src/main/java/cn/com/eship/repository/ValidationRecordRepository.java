package cn.com.eship.repository;

import cn.com.eship.models.ValidationRecord;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface ValidationRecordRepository extends CrudRepository<ValidationRecord, String>, JpaSpecificationExecutor<ValidationRecord> {
    @Modifying
    @Query("update ValidationRecord set msg = ?2,status = 1 where phoneNum = ?1 and status = null")
    public void updateValidationRecordStatus(String phoneNum, String msg);

    public List<ValidationRecord> findBySessionId(String sessionId);

    @Modifying
    @Query("update ValidationRecord set status = 0 where sessionId = ?1 and status = null")
    public void updateStastusBySessionId(String sessionId);
}
