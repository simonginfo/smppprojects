package cn.com.eship.repository;

import cn.com.eship.models.Phone;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PhoneRepository extends CrudRepository<Phone, String>, JpaSpecificationExecutor<Phone> {
    public Page<Phone> findAll(Pageable pageable);

    @Modifying
    @Query("delete from Phone where phoneNum = ?1")
    public void deleteByPhoneNum(String phoneNum);
}
