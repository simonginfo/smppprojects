package cn.com.eship;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SpringBootApplication
public class SjjcserverApplication {

	public static void main(String[] args) throws Exception {
		SpringApplication.run(SjjcserverApplication.class, args);
	}
}
