package cn.com.eship;

import org.apache.commons.codec.binary.Hex;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MyTests {
    public static void main(String[] args) throws Exception {
//        String key = md5(md5("MZ1ntzW1").toLowerCase() + "20180819182205").toLowerCase();
        String key = md5(md5("123456").toLowerCase() + "20130806102030").toLowerCase();
        System.out.println("cd6e1aa6b89e8e413867b33811e70153".equals(key));
    }


    public static String md5(String inStr) {
        try {
            MessageDigest currentAlgorithm = MessageDigest.getInstance("MD5");
            currentAlgorithm.reset();
            currentAlgorithm.update(inStr.getBytes());
            byte[] hash = currentAlgorithm.digest();
            return Hex.encodeHexString(hash);
        } catch (NoSuchAlgorithmException e) {
            return null;
        }
    }
}
