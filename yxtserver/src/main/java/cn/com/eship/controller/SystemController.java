package cn.com.eship.controller;

import org.apache.commons.codec.binary.Hex;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("system")
public class SystemController {

    @RequestMapping("send")
    @ResponseBody
    public String sender(String phone,String content) throws Exception {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        String timeStr = simpleDateFormat.format(new Date());
        //4f4faf5f3bbac1857e1902c748c44945  MZ1ntzW1   zx_gateway1
        String key = md5(md5("zx_gateway1").toLowerCase() + timeStr).toLowerCase();

        CloseableHttpClient httpCilent = HttpClients.createDefault();//Creates CloseableHttpClient instance with default configuration.

        HttpPost post = new HttpPost("http://59.110.12.230:10000/send/v3/api");
        // 创建参数队列
        List<NameValuePair> formparams = new ArrayList<NameValuePair>();
        formparams.add(new BasicNameValuePair("account", "18100320"));
        formparams.add(new BasicNameValuePair("seed", simpleDateFormat.format(new Date())));
        formparams.add(new BasicNameValuePair("key", key));
        formparams.add(new BasicNameValuePair("phone", phone));
        formparams.add(new BasicNameValuePair("content", content));
        formparams.add(new BasicNameValuePair("typeid", "227"));
        HttpEntity entity1 = new UrlEncodedFormEntity(formparams, "utf-8");
        post.setEntity(entity1);
        CloseableHttpResponse httpResponse = httpCilent.execute(post);

        String result = EntityUtils.toString(httpResponse.getEntity());

        httpResponse.close();
        httpCilent.close();
        return result;
    }


    public static String md5(String inStr) {
        try {
            MessageDigest currentAlgorithm = MessageDigest.getInstance("MD5");
            currentAlgorithm.reset();
            currentAlgorithm.update(inStr.getBytes());
            byte[] hash = currentAlgorithm.digest();
            return Hex.encodeHexString(hash);
        } catch (NoSuchAlgorithmException e) {
            return null;
        }
    }
}
