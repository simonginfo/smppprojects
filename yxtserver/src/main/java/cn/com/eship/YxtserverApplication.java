package cn.com.eship;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class YxtserverApplication {

    public static void main(String[] args) {
        SpringApplication.run(YxtserverApplication.class, args);
    }
}
