package org.sgip.web.sgipserver.repository;

import org.sgip.web.sgipserver.model.SendTime;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SendTimeRepository extends CrudRepository<SendTime, String>, JpaSpecificationExecutor<SendTime> {
    @Query(value = "select count(1) from send_time where send_key = ?1",nativeQuery = true)
    public Long findCountBySendKey(String sendKey);

    @Query(value = "select count(1) from send_time where phone_num = ?1",nativeQuery = true)
    public Long findCountByPhoneNum(String phoneNum);

    @Modifying
    public int deleteAllBySendKey(String sendKey);
}
