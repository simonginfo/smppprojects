package org.sgip.web.sgipserver.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "env_dict")
public class EnvDict {
    private String id;
    private String dictKey;
    private String dictName;
    private String dictValue;
    private String dictType;
    @Id
    @Column(name = "id", nullable = false, length = 32)
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Basic
    @Column(name = "dict_key", length = 1000, nullable = false)
    public String getDictKey() {
        return dictKey;
    }

    public void setDictKey(String dictKey) {
        this.dictKey = dictKey;
    }

    @Basic
    @Column(name = "dict_name", length = 100, nullable = false)
    public String getDictName() {
        return dictName;
    }

    public void setDictName(String dictName) {
        this.dictName = dictName;
    }

    @Basic
    @Column(name = "dict_value", length = 1000, nullable = false)
    public String getDictValue() {
        return dictValue;
    }

    public void setDictValue(String dictValue) {
        this.dictValue = dictValue;
    }

    @Basic
    @Column(name = "dict_type", length = 100, nullable = false)
    public String getDictType() {
        return dictType;
    }

    public void setDictType(String dictType) {
        this.dictType = dictType;
    }
}
