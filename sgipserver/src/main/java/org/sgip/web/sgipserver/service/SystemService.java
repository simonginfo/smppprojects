package org.sgip.web.sgipserver.service;

import org.sgip.web.sgipserver.model.EnvDict;
import org.sgip.web.sgipserver.model.SendRule;
import org.sgip.web.sgipserver.model.SendTimeCounter;
import org.sgip.web.sgipserver.model.User;

import java.util.List;
import java.util.Map;

public interface SystemService {
	public String sendMS(String phoneNum,String key,String content) throws Exception;
	public String saveSendRule(SendRule sendRule);

	public List<EnvDict> findEnvDictList(EnvDict envDict);

	public void updateEnvDictValue(EnvDict envDict1);


	public EnvDict findEnvDictById(String id);

	public List<SendRule> findSendRuleList(SendRule sendRule);

	public void updateSendRule(SendRule sendRule);

	public void addSendRule(SendRule sendRule);

	public void deleteSendRuleById(String id);

	public SendRule findSendRuleById(String id);

	public SendTimeCounter counterSendTimeByKey(String key);

	public void deleteSendTimeByKey(String key);

	public User login(User user);

}
