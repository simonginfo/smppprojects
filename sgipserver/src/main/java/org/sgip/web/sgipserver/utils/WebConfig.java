package org.sgip.web.sgipserver.utils;

import org.sgip.web.sgipserver.model.User;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class WebConfig extends HandlerInterceptorAdapter {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        User userInfo = (User) request.getSession().getAttribute("userInfo");
        if (userInfo == null){
            response.sendRedirect("/loginPage");
            return false;
        }
        return true;

    }
}
