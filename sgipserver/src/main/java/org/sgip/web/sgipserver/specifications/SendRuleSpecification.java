package org.sgip.web.sgipserver.specifications;


import org.apache.commons.lang3.StringUtils;
import org.sgip.web.sgipserver.model.EnvDict;
import org.sgip.web.sgipserver.model.SendRule;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;


public class SendRuleSpecification implements Specification<SendRule> {
    private SendRule sendRule;

    public SendRuleSpecification(SendRule sendRule) {
        this.sendRule = sendRule;
    }

    @Override
    public Predicate toPredicate(Root<SendRule> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
        if (sendRule != null) {
            List<Predicate> predicates = new ArrayList<>();

            Path<String> keyStr = root.get("keyStr");
            if (StringUtils.isNotBlank(sendRule.getKeyStr())) {
                predicates.add(criteriaBuilder.equal(keyStr, sendRule.getKeyStr()));
            }
            return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
        }
        return null;

    }
}