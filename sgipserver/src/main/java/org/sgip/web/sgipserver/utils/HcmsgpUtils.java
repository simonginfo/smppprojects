package org.sgip.web.sgipserver.utils;

import com.zjhc.smproxy.util.Args;

import java.util.Map;

public class HcmsgpUtils {
    public static Args convertMapToArgs(Map<String, String> map) {
        Args args = new Args();
        for (Map.Entry<String, String> entry : map.entrySet()) {
            args.set(entry.getKey(), entry.getValue());
        }
        return args;
    }
}
