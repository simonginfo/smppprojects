package org.sgip.web.sgipserver.repository;

import org.sgip.web.sgipserver.model.EnvDict;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EnvDictRepository extends CrudRepository<EnvDict, String>, JpaSpecificationExecutor<EnvDict> {
   @Query("SELECT envDict FROM EnvDict envDict WHERE envDict.id = ?1")
    public EnvDict findEnvDictById(String id);


    public EnvDict findByDictKey(String dictKey);

}
