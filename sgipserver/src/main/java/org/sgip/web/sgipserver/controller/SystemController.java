package org.sgip.web.sgipserver.controller;

import org.apache.commons.lang3.StringUtils;
import org.sgip.web.sgipserver.model.EnvDict;
import org.sgip.web.sgipserver.model.SendRule;
import org.sgip.web.sgipserver.model.SendTimeCounter;
import org.sgip.web.sgipserver.model.User;
import org.sgip.web.sgipserver.service.SystemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Repository;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/")
public class SystemController {
    @Autowired
    private SystemService systemService;

    @RequestMapping("sender")
    public @ResponseBody
    Map<String, String> sender(String phoneNum, String sendKey, String content) throws Exception {
        String result = systemService.sendMS(phoneNum, sendKey, content);
        Map<String, String> map = new HashMap<>();
        map.put("code", result);
        map.put("phoneNum", phoneNum);
        map.put("key", sendKey);
        if ("0".equals(result)) {
            map.put("msg", "发送已超过最大限制或者规则没有此key记录");
        }
        if ("1".equals(result)) {
            map.put("msg", "调用异常");
        }
        if ("2".equals(result)) {
            map.put("msg", "发送成功");
        }
        if ("3".equals(result)) {
            map.put("msg", "发送失败");
        }
        return map;
    }

    @RequestMapping("saveSendRule")
    public @ResponseBody
    Map<String, String> saveSendRule(String sendKey, String maxNum) {
        SendRule sendRule = new SendRule();
        sendRule.setKeyStr(sendKey);
        sendRule.setMaxNum(Long.valueOf(maxNum));
        String result = systemService.saveSendRule(sendRule);
        Map<String, String> map = new HashMap<>();
        map.put("code", result);
        map.put("key", sendKey);
        if ("0".equals(result)) {
            map.put("msg", "信息不能为空");
        }
        if ("1".equals(result)) {
            map.put("msg", "库里已有该key规则");
        }
        if ("2".equals(result)) {
            map.put("msg", "保存记录成功");
        }
        return map;
    }


    @RequestMapping("indexPage")
    public String toIndexPage() {
        return "index";
    }

    @RequestMapping("envDictListPage")
    public String toEnvDictListPage(EnvDict envDict, Model model) {
        List<EnvDict> envDictList = systemService.findEnvDictList(envDict);
        model.addAttribute("envDictList", envDictList);
        model.addAttribute("envDict1", envDict);
        return "envDictList";
    }


    @RequestMapping("updateEnvDictPage")
    public String toUpdateEnvDictPage(String id, Model model) {
        EnvDict envDict = systemService.findEnvDictById(id);
        model.addAttribute("envDict", envDict);
        return "updateEnvDictValue";
    }

    @RequestMapping("updateEnvDict")
    public String updateEnvDict(EnvDict envDict) {
        systemService.updateEnvDictValue(envDict);
        return "redirect:/envDictListPage";
    }

    @RequestMapping("sendRuleListPage")
    public String toSendRuleListPage(SendRule sendRule, Model model) {
        List<SendRule> sendRuleList = systemService.findSendRuleList(sendRule);
        model.addAttribute("sendRuleList", sendRuleList);
        model.addAttribute("sendRule", sendRule);
        return "sendRuleList";
    }


    @RequestMapping("deleteSendRuleById")
    public String deleteSendRuleById(String id) {
        systemService.deleteSendRuleById(id);
        return "redirect:/sendRuleListPage";
    }

    @RequestMapping("updateSendRulePage")
    public String toUpdateSendRulePage(String id, Model model) {
        SendRule sendRule = systemService.findSendRuleById(id);
        model.addAttribute("sendRule", sendRule);
        return "updateSendRule";
    }

    @RequestMapping("updateSendRule")
    public String updateSendRule(SendRule sendRule) {
        systemService.updateSendRule(sendRule);
        return "redirect:/sendRuleListPage";
    }

    @RequestMapping("saveSendRule2")
    public String saveSendRule(SendRule sendRule) {
        systemService.addSendRule(sendRule);
        return "redirect:/sendRuleListPage";
    }


    @RequestMapping("saveSendRulePage")
    public String toSaveSendRulePage() {
        return "addSendRule";
    }


    @RequestMapping("counterSendTimePage")
    public String counterSendTime() {
        return "sendTimeCounter";
    }


    @RequestMapping("counterSendTime")
    public @ResponseBody
    SendTimeCounter counterSendTime(String key) {
        SendTimeCounter sendTimeCounter = systemService.counterSendTimeByKey(key);
        return sendTimeCounter;
    }

    @RequestMapping("deleteSendTimeByKey")
    public String deleteSendTimeByKey(String key) {
        systemService.deleteSendTimeByKey(key);
        return "sendTimeCounter";
    }


    @RequestMapping("loginPage")
    public String toLoginPage(){
        return "login";
    }

    @RequestMapping(value = "login", method = RequestMethod.POST)
    public String login(User user, HttpSession session,Model model){
        if (user != null && StringUtils.isNotBlank(user.getUserAccount()) && StringUtils.isNotBlank(user.getPassword())){
            User userinfo = systemService.login(user);
            if (userinfo != null) {
                session.setAttribute("userInfo",userinfo);
                return "redirect:/indexPage";

            }else {
                model.addAttribute("errorMsg","用户名或者密码错误");
            }

        }else {
            model.addAttribute("errorMsg","用户名或者密码不能为空");
        }
        return "login";

    }
}
