package org.sgip.web.sgipserver.service.impl;

import com.zjhc.smproxy.comm.smgp.message.SMGPSubmitMessage;
import com.zjhc.smproxy.comm.smgp.message.SMGPSubmitRespMessage;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.sgip.web.sgipserver.model.*;
import org.sgip.web.sgipserver.repository.EnvDictRepository;
import org.sgip.web.sgipserver.repository.SendRuleRepository;
import org.sgip.web.sgipserver.repository.SendTimeRepository;
import org.sgip.web.sgipserver.repository.UserRepository;
import org.sgip.web.sgipserver.service.SystemService;
import org.sgip.web.sgipserver.specifications.EnvDictSpecification;
import org.sgip.web.sgipserver.specifications.SendRuleSpecification;
import org.sgip.web.sgipserver.utils.HcmsgpSender;
import org.sgip.web.sgipserver.utils.HcmsgpUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
@EnableTransactionManagement
public class SystemServiceImpl implements SystemService {
    @Autowired
    private SendTimeRepository sendTimeRepository;
    @Autowired
    private SendRuleRepository sendRuleRepository;
    @Autowired
    private EnvDictRepository envDictRepository;
    @Autowired
    private UserRepository userRepository;

    @Override
    public String sendMS(String phoneNum, String key, String content) throws Exception {
        SendRule sendRule = sendRuleRepository.findOneByKeyStr(key);
        EnvDict envDict = envDictRepository.findByDictKey("phoneMaxSendTime");
        Long currentSendTime = sendTimeRepository.findCountBySendKey(key);
        Long currentPhoneNumSendTime = sendTimeRepository.findCountByPhoneNum(phoneNum);
        if (sendRule != null && currentSendTime < sendRule.getMaxNum() && envDict != null && StringUtils.isNoneBlank(envDict.getDictValue()) && currentPhoneNumSendTime < Long.valueOf(envDict.getDictValue())) {
            try {
                String url="http://222.169.209.203:8086/sender/senderMsg?src=" + phoneNum + "&msg=" + content;
                //1.使用默认的配置的httpclient
                CloseableHttpClient client = HttpClients.createDefault();
                //2.使用get方法
                HttpGet httpGet = new HttpGet(url);
                client.execute(httpGet);
                //插入统计次数
                SendTime sendTime = new SendTime();
                sendTime.setId(UUID.randomUUID().toString().replaceAll("-", ""));
                sendTime.setSendDate(new Date());
                sendTime.setSendKey(key);
                sendTime.setPhoneNum(phoneNum);
                sendTime.setSendContent(content);
                sendTimeRepository.save(sendTime);
                return "2";
            } catch (Exception e) {
                e.printStackTrace();
                return "1";
            }

        }
        return "0";
    }

    @Override
    public String saveSendRule(SendRule sendRule) {
        if (sendRule == null) {
            return "0";
        } else {
            try {
                sendRuleRepository.save(sendRule);
            } catch (Exception e) {
                return "1";
            }
        }

        return "2";
    }

    @Override
    public List<EnvDict> findEnvDictList(EnvDict envDict) {
        EnvDictSpecification envDictSpecification = new EnvDictSpecification(envDict);
        List<EnvDict> envDictList = envDictRepository.findAll(envDictSpecification);
        return envDictList;
    }

    @Override
    @Transactional
    public void updateEnvDictValue(EnvDict envDict1) {
        EnvDict envDict = envDictRepository.findEnvDictById(envDict1.getId());
        envDict.setDictValue(envDict1.getDictValue());
        envDict.setDictName(envDict1.getDictName());
        envDictRepository.save(envDict);
    }

    @Override
    public EnvDict findEnvDictById(String id) {
        EnvDict envDict = envDictRepository.findEnvDictById(id);
        return envDict;
    }

    @Override
    public List<SendRule> findSendRuleList(SendRule sendRule) {
        SendRuleSpecification sendRuleSpecification = new SendRuleSpecification(sendRule);
        List<SendRule> sendRuleList = sendRuleRepository.findAll(sendRuleSpecification);
        return sendRuleList;
    }

    @Override
    @Transactional
    public void updateSendRule(SendRule sendRule) {
        SendRule sendRule1 = sendRuleRepository.findOneByKeyStr(sendRule.getKeyStr());
        sendRule1.setMaxNum(sendRule.getMaxNum());
        sendRuleRepository.save(sendRule1);
    }

    @Override
    public void addSendRule(SendRule sendRule) {
        sendRuleRepository.save(sendRule);
    }

    @Override
    @Transactional
    public void deleteSendRuleById(String id) {
        SendRule sendRule1 = sendRuleRepository.findOneByKeyStr(id);
        sendRuleRepository.delete(sendRule1);
    }

    @Override
    public SendRule findSendRuleById(String id) {
        SendRule sendRule = sendRuleRepository.findOneByKeyStr(id);
        return sendRule;
    }

    private Map<String, String> convertEnvDictListToSendConfigMap(List<EnvDict> envDictList) {
        Map<String, String> map = new HashMap<>();
        for (EnvDict envDict : envDictList) {
            if ("1".equals(envDict.getDictType())) {
                map.put(envDict.getDictKey(), envDict.getDictValue());
            }

        }
        return map;
    }


    private Map<String, String> convertEnvDictListToEnvConfigMap(List<EnvDict> envDictList) {
        Map<String, String> map = new HashMap<>();
        for (EnvDict envDict : envDictList) {
            if ("0".equals(envDict.getDictType())) {
                map.put(envDict.getDictKey(), envDict.getDictValue());
            }
        }
        return map;
    }


    @Override
    public SendTimeCounter counterSendTimeByKey(String key) {
        Long times = sendTimeRepository.findCountBySendKey(key);
        if (times != null) {
            SendTimeCounter sendTimeCounter = new SendTimeCounter();
            sendTimeCounter.setKey(key);
            sendTimeCounter.setSendTimes(times);
            return sendTimeCounter;
        }

        return null;
    }

    @Override
    @Transactional
    public void deleteSendTimeByKey(String key) {
        sendTimeRepository.deleteAllBySendKey(key);
    }


    @Override
    public User login(User user) {
        User userResult = userRepository.findUserByUserAccountAndPassword(user.getUserAccount(), user.getPassword());
        return userResult;
    }

}
