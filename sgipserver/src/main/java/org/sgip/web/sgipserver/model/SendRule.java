package org.sgip.web.sgipserver.model;

import javax.persistence.*;

@Entity
@Table(name = "send_rule")
public class SendRule {
    private String keyStr;
    private Long maxNum;


    @Id
    @Column(name = "key_str", nullable = false, length = 500)
    public String getKeyStr() {
        return keyStr;
    }

    public void setKeyStr(String keyStr) {
        this.keyStr = keyStr;
    }

    @Basic
    @Column(name = "max_num", nullable = false)
    public Long getMaxNum() {
        return maxNum;
    }

    public void setMaxNum(Long maxNum) {
        this.maxNum = maxNum;
    }
}
