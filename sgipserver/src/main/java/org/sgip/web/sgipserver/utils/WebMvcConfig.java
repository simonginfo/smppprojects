package org.sgip.web.sgipserver.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

@SpringBootConfiguration
public class WebMvcConfig extends WebMvcConfigurationSupport {
    @Autowired
    private WebConfig webConfig;
    @Override
    protected void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(webConfig).addPathPatterns("/**").excludePathPatterns("/login","/loginPage","/sender");

    }
}
