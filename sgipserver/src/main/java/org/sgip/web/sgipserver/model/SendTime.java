package org.sgip.web.sgipserver.model;

import javax.persistence.*;
import java.util.Date;
@Entity
@Table(name = "send_time")
public class SendTime {
    private String id;
    private String sendKey;
    private String phoneNum;
    private String sendContent;
    private Date sendDate;

    @Id
    @Column(name = "id", nullable = false, length = 32)
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    @Basic
    @Column(name = "send_key", nullable = false, length = 500)
    public String getSendKey() {
        return sendKey;
    }

    public void setSendKey(String sendKey) {
        this.sendKey = sendKey;
    }

    @Basic
    @Column(name = "send_date", nullable = false)
    public Date getSendDate() {
        return sendDate;
    }

    public void setSendDate(Date sendDate) {
        this.sendDate = sendDate;
    }

	/**
	 * @return the phoneNum
	 */
    @Basic
    @Column(name = "phone_num", length=100, nullable = false)
	public String getPhoneNum() {
		return phoneNum;
	}

	/**
	 * @param phoneNum the phoneNum to set
	 */
	public void setPhoneNum(String phoneNum) {
		this.phoneNum = phoneNum;
	}

	/**
	 * @return the sendContent
	 */
    @Basic
    @Column(name = "send_content", length=2000, nullable = false)
	public String getSendContent() {
		return sendContent;
	}

	/**
	 * @param sendContent the sendContent to set
	 */
	public void setSendContent(String sendContent) {
		this.sendContent = sendContent;
	}
}
