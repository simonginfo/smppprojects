package org.sgip.web.sgipserver.model;

import java.io.Serializable;

public class SendTimeCounter implements Serializable {
    private String key;
    private Long sendTimes;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Long getSendTimes() {
        return sendTimes;
    }

    public void setSendTimes(Long sendTimes) {
        this.sendTimes = sendTimes;
    }
}
