package org.sgip.web.sgipserver;

import org.springframework.beans.factory.ListableBeanFactory;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.http.HttpMessageConverters;
import org.springframework.boot.autoconfigure.web.ResourceProperties;

@SpringBootApplication
public class SgipserverApplication {

	public static void main(String[] args) {
		SpringApplication.run(SgipserverApplication.class, args);
	}
}
