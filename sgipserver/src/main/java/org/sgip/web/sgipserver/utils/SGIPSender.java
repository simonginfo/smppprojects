package org.sgip.web.sgipserver.utils;

import java.util.*;
import java.util.Date;
import java.sql.*;
import java.io.*;

import com.huawei.smproxy.SGIPSMProxy;

import com.huawei.smproxy.comm.sgip.*;
import com.huawei.smproxy.comm.sgip.message.*;
import com.huawei.insa2.util.*;
public class SGIPSender extends SGIPSMProxy {

    public SGIPSender(Map arg, String host, Integer port) {
        super(arg);
        startService(host, port);
    }

    public void OnTerminate() {
        System.out.println("Connection have been breaked! ");
    }

    public SGIPSubmitRepMessage send(SGIPSubmitMessage msg) {
        if (msg == null) {
            return null;
        }
        SGIPSubmitRepMessage reportMsg = null;

        try {
            reportMsg = (SGIPSubmitRepMessage) super.send(msg);
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return reportMsg;
    }
}
