package org.sgip.web.sgipserver.repository;

import org.sgip.web.sgipserver.model.SendRule;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SendRuleRepository extends CrudRepository<SendRule, String>, JpaSpecificationExecutor<SendRule> {
    @Query("SELECT sendRule FROM SendRule sendRule where sendRule.keyStr = ?1")
    public SendRule findOneByKeyStr(String key);
}
