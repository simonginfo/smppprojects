package org.sgip.web.sgipserver.specifications;


import org.apache.commons.lang3.StringUtils;
import org.sgip.web.sgipserver.model.EnvDict;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;


public class EnvDictSpecification implements Specification<EnvDict> {
    private EnvDict envDict;

    public EnvDictSpecification(EnvDict envDict) {
        this.envDict = envDict;
    }

    @Override
    public Predicate toPredicate(Root<EnvDict> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
        if (envDict != null) {
            List<Predicate> predicates = new ArrayList<>();

            Path<String> dictType = root.get("dictType");
            Path<String> dictKey = root.get("dictKey");
            if (StringUtils.isNotBlank(envDict.getDictType())) {
                predicates.add(criteriaBuilder.equal(dictType, envDict.getDictType()));
            }
            if (StringUtils.isNotBlank(envDict.getDictKey())) {
                predicates.add(criteriaBuilder.like(dictKey, "%" + envDict.getDictKey() + "%"));
            }
            return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
        }
        return null;

    }
}