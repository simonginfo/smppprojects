package org.sgip.web.sgipserver.repository;

import org.sgip.web.sgipserver.model.User;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<User, String>, JpaSpecificationExecutor<User> {
    @Query("SELECT user FROM User user where user.userAccount = ?1 and user.password = ?2")
    public User findUserByUserAccountAndPassword(String userAccount,String password);
}
